jQuery(document).ready(function(e) {
jQuery('.navbar-toggler').on('click', function(e) {
jQuery('body').toggleClass('nav-open');
e.stopPropagation();
});


});

jQuery(document).click(function(e) {
jQuery('body').removeClass('nav-open');
});


// On Scroll add class 
jQuery(window).scroll(function() {    
    var scroll = jQuery(window).scrollTop();
    if (scroll >= 150) {
        jQuery("body").addClass("darkHeader");
    }
    if (scroll <= 50) {
        jQuery("body").removeClass("darkHeader");
    }
});

// pop-slider

jQuery('.best-slider').slick({
slidesToShow: 1,
slidesToScroll: 1,
arrows: false,
dots: true,
  centerMode: true,
  centerPadding: '60px',
autoplay:false,
mobileFirst:true,
responsive: [
{
breakpoint: 479.98,
settings: {
slidesToShow: 2
}
},
{
breakpoint: 768.98,
settings: {
  arrows: true,
  centerMode: true,
  centerPadding: '60px',
slidesToShow: 2,
dots: true
}
},
{
breakpoint: 1199.98,
settings: {
slidesToShow: 1,
centerMode: true,
  centerPadding: '360px',
slidesToScroll: 1,
dots: true
}
}
]
});

// team slider

jQuery('.team-slider').slick({
  dots: true,
  infinite: false,
  arrows: false,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});


// testimonial slider

jQuery('.testimonial-slider').slick({
  dots: true,
  infinite: false,
  arrows: false,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});


// logo slider

jQuery('.logo-slider').slick({
  infinite: true,
  slidesToShow: 1,
  arrows: false,
  dots: true,
  slidesToScroll: 1
});



// product detail slider

jQuery('.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: true,
  fade: true,
  asNavFor: '.slider-nav',
   autoplay: false
});
jQuery('.slider-nav').slick({
  slidesToShow: 4,
  vertical: true,
  arrows: false,
  slidesToScroll: 1,
  asNavFor: '.slider-for',
  dots: false

});


jQuery(".card").click(function(){
  jQuery(this).toggleClass('arrow');
  jQuery(this).siblings().removeClass('arrow');
});


// Digital Mega-Menu JS
jQuery('.dropdown').hover(function(e){
  jQuery('body').addClass('digital-dropdown');
});
jQuery(document).on("click", function(e) {
  if (jQuery(e.target).is(".dropdown") === false) {
    jQuery("body").removeClass("digital-dropdown");
  }
});

// accordian card

jQuery('.inner-accordion .inner-card').on('click', function(e) {
    jQuery(this).addClass('arrow');
    jQuery(this).siblings().removeClass('arrow');
  });



//same height 
      jQuery(document).ready(function(){
    jQuery('.advertising-service .row').each(function(){ 

      var highestBox = 0;

      jQuery('.col-4', this).each(function(){

        if(jQuery(this).height() > highestBox) {
          highestBox = jQuery(this).height(); 
        }
      }); 

      jQuery('.col-4', this).height(highestBox);
                    
    });
     });