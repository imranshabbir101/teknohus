import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }
  bestslider = [
    {img: "assets/portfolio1.png", name: "mashal", designation: "frontend"},
    {img: "assets/best1.jpg", name: "mashal", designation: "frontend"},
    {img: "assets/best1.jpg", name: "mashal", designation: "frontend"},
    {img: "assets/best1.jpg", name: "mashal", designation: "frontend"}
  ];
  slideConfigbest = {
    "slidesToShow": 3, 
    "slidesToScroll": 1,
    "arrows": false,
    "dots": true,
    "centerMode": true,
  "centerPadding": '100px'
  };
  ngOnInit(): void {
  }
  heroslider =[
    {img: "assets/team1.jpg", name: "Martin", designation: "frontend"},
    {img: "assets/team1.jpg", name: "Martin", designation: "frontend"},
    {img: "assets/team1.jpg", name: "Martin", designation: "frontend"},
    {img: "assets/team1.jpg", name: "Martin", designation: "frontend"},
    {img: "assets/team1.jpg", name: "Martin", designation: "frontend"},
    {img: "assets/team1.jpg", name: "Martin", designation: "frontend"},
    {img: "assets/team1.jpg", name: "Martin", designation: "frontend"}
  ];
  slideConfighero={
    "slidesToShow": 4, 
    "slidesToScroll": 1,
    "arrows": false,
    "dots": true
  };
  testimonialslider =[
    {img: "assets/testimonial-img.jpg", name: "Julie Lane"},
    {img: "assets/testimonial-img.jpg", name: "Julie Lane"},
    {img: "assets/testimonial-img.jpg", name: "Julie Lane"},
    {img: "assets/testimonial-img.jpg", name: "Julie Lane"},
    {img: "assets/testimonial-img.jpg", name: "Julie Lane"},
    {img: "assets/testimonial-img.jpg", name: "Julie Lane"},
    {img: "assets/testimonial-img.jpg", name: "Julie Lane"}
  ]
  slideConfigtestimonial ={
    "slidesToShow": 4, 
    "slidesToScroll": 1,
    "arrows": false,
    "dots": true
  };

  logolistslider = [
    {img: "assets/logo1.png"},
    {img: "assets/logo2.png"},
    {img: "assets/logo3.png"},
    {img: "assets/logo4.png"},
    {img: "assets/logo5.png"},
    {img: "assets/logo6.png"},
    {img: "assets/logo7.png"},
    {img: "assets/logo8.png"}
  ]
  slideConfiglogolist ={
    "slidesToShow": 1, 
    "slidesToScroll": 1,
    "arrows": false,
    "dots": true
  }
}
