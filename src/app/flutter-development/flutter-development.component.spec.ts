import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlutterDevelopmentComponent } from './flutter-development.component';

describe('FlutterDevelopmentComponent', () => {
  let component: FlutterDevelopmentComponent;
  let fixture: ComponentFixture<FlutterDevelopmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlutterDevelopmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlutterDevelopmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
