import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-flutter-development',
  templateUrl: './flutter-development.component.html',
  styleUrls: ['./flutter-development.component.css']
})
export class FlutterDevelopmentComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  logolistslider = [
    {img: "assets/logo1.png"},
    {img: "assets/logo2.png"},
    {img: "assets/logo3.png"},
    {img: "assets/logo4.png"},
    {img: "assets/logo5.png"},
    {img: "assets/logo6.png"},
    {img: "assets/logo7.png"},
    {img: "assets/logo8.png"}
  ]
  slideConfiglogolist ={
    "slidesToShow": 1, 
    "slidesToScroll": 1,
    "arrows": false,
    "dots": true
  }
}
