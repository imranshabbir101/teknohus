import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-seo',
  templateUrl: './seo.component.html',
  styleUrls: ['./seo.component.css']
})
export class SeoComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  logolistslider = [
    {img: "assets/logo1.png"},
    {img: "assets/logo2.png"},
    {img: "assets/logo3.png"},
    {img: "assets/logo4.png"},
    {img: "assets/logo5.png"},
    {img: "assets/logo6.png"},
    {img: "assets/logo7.png"},
    {img: "assets/logo8.png"}
  ]
  slideConfiglogolist ={
    "slidesToShow": 1, 
    "slidesToScroll": 1,
    "arrows": false,
    "dots": true
  }

}
