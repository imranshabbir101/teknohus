import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopifyServicesComponent } from './shopify-services.component';

describe('ShopifyServicesComponent', () => {
  let component: ShopifyServicesComponent;
  let fixture: ComponentFixture<ShopifyServicesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShopifyServicesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopifyServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
