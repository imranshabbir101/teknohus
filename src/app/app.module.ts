import { BrowserModule } from '@angular/platform-browser';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { NgModule } from '@angular/core';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { PpcComponent } from './ppc/ppc.component';
import { SeoComponent } from './seo/seo.component';
import { ShopifyServicesComponent } from './shopify-services/shopify-services.component';
import { MagentoComponent } from './magento/magento.component';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { ServicesComponent } from './services/services.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ContactFormComponent } from './contact-form/contact-form.component';
import { HttpClientModule } from '@angular/common/http';
import { ContactService } from './contact.service';
import { FlutterDevelopmentComponent } from './flutter-development/flutter-development.component';
import { UnityAppComponent } from './unity-app/unity-app.component';
import { PythonComponent } from './python/python.component';
import { NodeComponent } from './node/node.component';
import { ReactjsComponent } from './reactjs/reactjs.component';
import { BlockchainComponent } from './blockchain/blockchain.component';
import { WorkComponent } from './work/work.component';
import { ClientsComponent } from './clients/clients.component';
import { WorkDetailComponent } from './work-detail/work-detail.component';
import { WordpressComponent } from './wordpress/wordpress.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    PpcComponent,
    SeoComponent,
    ShopifyServicesComponent,
    MagentoComponent,
    HomeComponent,
    ContactComponent,
    ServicesComponent,
    ContactFormComponent,
    FlutterDevelopmentComponent,
    UnityAppComponent,
    PythonComponent,
    NodeComponent,
    ReactjsComponent,
    BlockchainComponent,
    WorkComponent,
    ClientsComponent,
    WorkDetailComponent,
    WordpressComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SlickCarouselModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    ContactService,
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
