import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
import { MagentoComponent } from './magento/magento.component';
import { PpcComponent } from './ppc/ppc.component';
import { SeoComponent } from './seo/seo.component';
import { ServicesComponent } from './services/services.component';
import { ContactFormComponent } from './contact-form/contact-form.component';
import { FlutterDevelopmentComponent } from './flutter-development/flutter-development.component';
import { UnityAppComponent } from './unity-app/unity-app.component';
import { PythonComponent } from './python/python.component';
import { NodeComponent } from './node/node.component';
import { ReactjsComponent } from './reactjs/reactjs.component';
import { BlockchainComponent } from './blockchain/blockchain.component';
import { ShopifyServicesComponent } from './shopify-services/shopify-services.component';
import { WorkComponent} from './work/work.component';
import { ClientsComponent} from './clients/clients.component';
import { WorkDetailComponent} from './work-detail/work-detail.component';
import { WordpressComponent} from './wordpress/wordpress.component';
const routes: Routes = [
  { path: '',   redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'magento', component: MagentoComponent },
  { path: 'ppc', component: PpcComponent },
  { path: 'seo', component: SeoComponent },
  { path: 'services', component: ServicesComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'contact-form', component: ContactFormComponent },
  { path: 'flutter-development', component:  FlutterDevelopmentComponent },
  { path: 'unity-app', component:  UnityAppComponent },
  { path: 'python', component:  PythonComponent },
  { path: 'node', component:  NodeComponent },
  { path: 'reactjs', component:  ReactjsComponent },
  { path: 'blockchain', component:  BlockchainComponent},
  { path: 'shopify-services', component:  ShopifyServicesComponent},
  { path: 'work', component:  WorkComponent},
  { path: 'clients', component:  ClientsComponent},
  { path: 'work-detail', component:  WorkDetailComponent},
  { path: 'wordpress', component:  WordpressComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
